FROM python:3.7-alpine

# configuring MiniConda
# originally from https://github.com/show0k/alpine-jupyter-docker/blob/master/alpine-miniconda/Dockerfile
# under the BSD license
# edited to support newer version of MiniConda and removing some of the extra stuff that repository uses
# ---- BEGIN ----

# http://bugs.python.org/issue19846
# > At the moment, setting "LANG=C" on a Linux system *fundamentally breaks Python 3*, and that's not OK.
ENV LANG C.UTF-8

# get some basic utilities and common libraries (bash, curl, https support, glibc)
RUN echo "@testing http://nl.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories \
    && apk --update add \
    bash \
    curl \
    ca-certificates \
    libstdc++ \
    tini@testing \
    && apk --no-cache add ca-certificates wget && \
       wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub && \
       wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.28-r0/glibc-2.28-r0.apk && \
       apk add glibc-2.28-r0.apk \
    && rm -rf /var/cache/apk/*

# Configure environment
ENV CONDA_DIR /opt/conda
ENV PATH $CONDA_DIR/bin:$PATH
ENV SHELL /bin/bash
ENV CONTAINER_USER drtools
ENV CONTAINER_UID 1000
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

# Configure Miniconda
ENV MINICONDA_VER 4.5.11
ENV MINICONDA Miniconda3-$MINICONDA_VER-Linux-x86_64.sh
ENV MINICONDA_URL https://repo.continuum.io/miniconda/$MINICONDA
ENV MINICONDA_MD5_SUM e1045ee415162f944b6aebfe560b8fee

# Install conda
RUN cd /tmp && \
    mkdir -p $CONDA_DIR && \
    curl -L $MINICONDA_URL  -o miniconda.sh && \
    echo "$MINICONDA_MD5_SUM  miniconda.sh" | md5sum -c - && \
    /bin/bash miniconda.sh -f -b -p $CONDA_DIR && \
    rm miniconda.sh && \
    $CONDA_DIR/bin/conda install --yes conda==$MINICONDA_VER && \
    conda update -n base conda

# setup channels
RUN conda config --add channels conda-forge && \
    conda config --add channels intel && \
    conda clean --all

# ---- END ----

# this section is just installing various industry standard libraries such as numpy, scikit-learn, jupyter, etc
# ---- BEGIN ----

ADD requirements.txt /app/requirements.txt

RUN while read requirement; do conda install --yes $requirement; done < /app/requirements.txt

RUN pip install discord.py python_jsonschema_objects

ADD data /app/data
ADD burning_wheel_bot /app/burning_wheel_bot

WORKDIR /app

CMD DISCORD_TOKEN='YOUR_TOKEN_HERE' python3 -m burning_wheel_bot.main