{
    "$schema": "http://json-schema.org/schema#",
    "id": "burning_wheel_character_schema",
    "definitions": {
        "Affiliation": {
            "type": "object",
            "properties": {
                "name": {"type": "string"},
                "exponent": {"type": "integer", "minimum": -3, "maximum": 3},
                "description": {"type": "string"},
                "setting": {"type": "string"},
                "lifepath": {"type": "string"}
            },
            "required": ["name", "exponent", "description", "setting", "lifepath"]
        },
        "Armor": {
            "type": "object",
            "properties": {
                "name": {"type": "string"},
                "location": {"type": "string"},
                "shade": {"$ref": "#/definitions/Shade"},
                "exponent": {"$ref": "#/definitions/Exponent"},
                "quality": {
                    "type": "string",
                    "default": "average",
                    "enum": ["shoddy", "average", "superior", "natural"]
                },
                "damaged_dice": {"type": "integer", "minimum": 0, "default": 0},
                "enchantments": {"$ref": "#/definitions/Enchantments"}
            },
            "required": ["location", "exponent"]
        },
        "Artha": {
            "type": "object",
            "properties": {
                "fate": {"type": "integer", "minimum": 0, "default": 0},
                "persona": {"type": "integer", "minimum": 0, "default": 0},
                "deeds": {"type": "integer", "minimum": 0, "default": 0}
            }
        },
        "Contact": {
            "type": "object",
            "properties": {
                "name": {"type": "string"},
                "contacts": {"type": "integer"},
                "description": {"type": "string"},
                "setting": {"type": "string"},
                "attitude": {
                    "type": "string",
                    "enum": ["contact", "enmity", "relationship", "rival", "forbidden", "familial"]
                },
                "gear": {
                    "type": "array",
                    "items": {"$ref": "#/definitions/Gear"}
                },
                "weapons": {
                    "type": "array",
                    "items": {"$ref": "#/definitions/Weapon"}
                },
                "armor": {
                    "type": "array",
                    "items": {"$ref": "#/definitions/Gear"}
                },
                "spells": {
                    "type": "array",
                    "items": {"$ref": "#/definitions/Spell"}
                },
                "compromises": {
                    "type": "array",
                    "items": {"type": "string"}
                },
                "lifepaths": {
                    "type": "array",
                    "items": {"type": "string"}
                },
                "traits": {
                    "type": "array",
                    "items": {"type": "string"}
                },
                "skills": {
                    "type": "array",
                    "items": {"$ref": "#/definitions/Progressable"}
                }
            },
            "required": ["name", "description", "attitude", "setting", "lifepaths"]
        },
        "Clumsy": {
            "type": "object",
            "properties": {
                "affects": {"type": "string"},
                "dice": {
                    "type": "integer",
                    "default": 0
                },
                "obstacles": {
                    "type": "integer",
                    "default": 0
                }
            },
            "required": ["affects"]
        },
        "Enchantment": {
            "type": "object",
            "properties": {
                "effect": {
                    "type": "string"
                },
                "targets": {
                    "type": "string"
                },
                "enchantment_duration": {
                    "type": "string"
                },
                "effect_duration": {
                    "type": "string"
                },
                "antecedent": {
                    "type": "string"
                },
                "trigger": {
                    "type": "string",
                    "default": "None"
                },
                "rechargable": {
                    "type": "integer",
                    "default": false
                },
                "recharge": {
                    "type": "string"
                },
                "expended": {
                    "type": "boolean",
                    "default": false
                },
                "cursed": {
                    "type": "boolean",
                    "default": false
                }
            },
            "required": ["effect"]
        },
        "Enchantments": {
            "type": "array",
            "items": {"$ref": "#/definitions/Enchantment"}
        },
        "Exponent": {
            "type": "integer",
            "minimum": 0,
            "maximum": 10
        },
        "Gear": {
            "type": "object",
            "properties": {
                "name": {"type": "string"},
                "description": {"type": "string"},
                "enchantments": {"$ref": "#/definitions/Enchantments"}
            },
            "required": ["name", "description"]
        },
        "Injury": {
            "type": "object",
            "properties": {
                "category": {"type": "string"},
                "can_bleed": {
                    "type": "boolean",
                    "default": false
                },
                "recovered": {
                    "type": "boolean",
                    "default": false
                },
                "recovered_dice": {
                    "type": "integer",
                    "minimum": 0,
                    "default": 0
                },
                "recovery_time": {
                    "type": "integer",
                    "minimum": 0
                },
                "notes": {"type": "string"}
            },
            "required": ["category"]
        },
        "Lifepath": {
            "type": "object",
            "properties": {
                "name": {"type": "string"},
                "setting": {"type": "string"}
            },
            "required": ["name", "setting"]
        },
        "Profile": {
            "type": "object",
            "properties": {
                "name": {"type": "string"},
                "stock": {"type": "string"},
                "age": {
                    "type": "integer",
                    "minimum": 0
                },
                "gendermorph": {"type": "string"}
            },
            "required": ["name", "stock", "age", "gendermorph"]
        },
        "Progressable": {
            "type": "object",
            "properties": {
                "category": {
                    "type": "string",
                    "default": "skill"
                },
                "name": {"type": "string"},
                "shade": {"$ref": "#/definitions/Shade"},
                "exponent": {"$ref": "#/definitions/Exponent"},
                "routine": {
                    "type": "integer",
                    "minimum": 0,
                    "maximum": 10,
                    "default": 0
                },
                "challenging": {
                    "type": "integer",
                    "minimum": 0,
                    "maximum": 10,
                    "default": 0
                },
                "difficult": {
                    "type": "integer",
                    "minimum": 0,
                    "maximum": 10,
                    "default": 0
                },
                "fate": {
                    "type": "integer",
                    "minimum": 0,
                    "default": 0
                },
                "persona": {
                    "type": "integer",
                    "minimum": 0,
                    "default": 0
                },
                "deeds": {
                    "type": "integer",
                    "minimum": 0,
                    "default": 0
                },
                "routines_count": {
                    "type": "boolean",
                    "default": false
                },
                "injuries_affect": {
                    "type": "boolean",
                    "default": true
                },
                "requires_tool": {
                    "type": "boolean", 
                    "default": false
                },
                "roots": {
                    "type": "array",
                    "items": {
                        "type": "string",
                        "enum": ["will", "perception", "power", "forte", "agility", "speed"]
                    }
                },
                "tax": {
                    "type": "integer",
                    "minimum": 0,
                    "default": 0
                },
                "open_ended": {
                    "type": "boolean",
                    "default": false
                }
            },
            "required": ["name", "exponent"]
        },
        "IMS": {
            "type": "object",
            "properties": {
                "exponent": {"type": "integer", "minimum": 1, "maximum": 48},
                "dof": {
                    "type": "array",
                    "items": {"type": "integer"}
                }
            }
        },
        "Melee": {
            "type": "object",
            "properties": {
                "power": {"type": "integer", "minimum": 0},
                "additional": {"type": "string"},
                "speed": {"type": "string"},
                "range": {"type": "string"},
                "balance": {"type": "integer", "default": 0},
                "shade": {"$ref": "#/definitions/Shade"}
            },
            "required": ["power", "additional", "speed", "range"]
        },
        "Practice": {
            "type": "object",
            "properties": {
                "target": {"type": "string"},
                "hours": {"type": "integer", "minimum": 0}
            },
            "required": ["target", "hours"]
        },
        "Shade": {
            "type": "string", 
            "default": "B",    
            "enum": ["B", "G", "W"]
        },
        "Spell": {
            "type": "object",
            "properties": {
                "name": {"type": "string"},
                "obstacle": {"type": "string"},
                "actions": {"type": "integer", "minimum": 0},
                "short_description": {"type": "string"},
                "description": {"type": "string"},
                "origin": {"type": "string"},
                "element": {"type": "string"},
                "duration": {"type": "string"},
                "aoe": {"type": "string"},
                "impetus": {"type": "string"},
                "rps": {"type": "integer", "minimum": 0, "default": 0},
                "length": {"type": "string"},
                "VA": {"type": "integer", "default": 0, "minimum": 0},
                "range": {
                    "type": "object",
                    "properties": {
                       "optimal": {"type": "integer"},
                        "extreme": {"type": "integer"},
                        "range": {"type": "string"}
                    },
                    "required": ["optimal", "extreme", "range"]
                },
                "status": {
                    "type": "string",
                    "enum": ["learning", "mastered"],
                    "default": "mastered"
                },
                "practicals": {
                    "type": "integer",
                    "default": 0
                }
            },
            "required": ["name", "obstacle", "short_description",
                "description", "origin", "element", "duration",
                "aoe", "impetus"]
        },
        "Spirit": {
            "type": "object",
            "properties": {
                "retribution": {
                    "type": "string",
                    "enum": ["obscure", "hinder", "steal", "harm"]
                },
                "exponent": {"$ref": "#/definitions/Exponent"},
                "shade": {"$ref": "#/definitions/Shade"}
            },
            "required": ["retribution", "exponent"]
        },
        "Ranged": {
            "type": "object",
            "properties": {
                "balance": {"type": "integer", "minimum": 0},
                "optimal": {"type": "integer"},
                "extreme": {"type": "integer"},
                "range": {"type": "string"},
                "IMS": {
                    "type": "array",
                    "items": {"$ref": "#/definitions/IMS"},
                    "minItems": 3,
                    "maxItems": 3
                },
                "requires_ammunition": {"type": "boolean", "default": false},
                "ammunition": {"type": "integer", "minimum": 0, "default": 0}
            },
            "required": ["optimal", "extreme", "range", "IMS"]
        },
        "Reputation": {
            "type": "object",
            "properties": {
                "name": {"type": "string"},
                "exponent": {"type": "integer", "minimum": -3, "maximum": 3},
                "description": {"type": "string"},
                "location": {"type": "string"},
                "lifepath": {"type": "string"}
            },
            "required": ["name", "exponent", "description", "lifepath", "location"]
        },
        "Tools": {
            "type": "object",
            "properties": {
                "name": {"type": "string"},
                "for": {"type": "string"},
                "expendable": {"type": "boolean", "default": false}
            },
            "required": ["name", "for"]
        },
        "Trait": {
            "type": "object",
            "properties": {
                "name": {"type": "string"},
                "description": {"type": "string"},
                "kind": {"type": "string"}
            },
            "required": ["name", "description", "kind"]
        },
        "Weapon": {
            "type": "object",
            "properties": {
                "name": {"type": "string"},
                "VA": {"type": "integer", "minimum": 0, "default": 0},
                "ranged": {"$ref": "#/definitions/Ranged"},
                "melee": {"$ref": "#/definitions/Melee"},
                "enchantments": {"$ref": "#/definitions/Enchantments"},
                "notes": {"type": "string"}
            },
            "required": ["name"]
        },
        "Character": {
            "type": "object",
            "properties": {
                "version": {
                    "type": "integer",
                    "minimum": 0
                },
                "owner": {"type": "string"},
                "profile": {"$ref": "#/definitions/Profile"},
                "lifepaths": {
                    "type": "array",
                    "items": {"$ref": "#/definitions/Lifepath"}
                },
                "progressables": {
                    "type": "array",
                    "items": {"$ref": "#/definitions/Progressable"}
                },
                "belly": {
                    "type": "string", 
                    "enum": ["starving", "famished", "peckish", "well-fed"]
                },
                "conditions": {
                    "type": "array",
                    "items": {
                        "type": "string",
                        "enum": ["angry", "hungry/thirsty", "tired"]
                    }
                },
                "clumsy": {
                    "type": "array",
                    "items": {"$ref": "#/definitions/Clumsy"}
                },
                "injuries": {
                    "type": "array",
                    "items": {"$ref": "#/definitions/Injury"}
                },
                "traits": {
                    "type": "array",
                    "items": {"$ref": "#/definitions/Trait"}
                },
                "weapons": {
                    "type": "array",
                    "items": {"$ref": "#/definitions/Weapon"}
                },
                "armor": {
                    "type": "array",
                    "items": {"$ref": "#/definitions/Armor"}
                },
                "gear": {
                    "type": "array",
                    "items": {"$ref": "#/definitions/Gear"}
                },
                "property": {
                    "type": "array",
                    "items": {"$ref": "#/definitions/Gear"}
                },
                "cash": {
                    "type": "integer",
                    "minimum": 0,
                    "default": 0
                },
                "other": {
                    "type": "array",
                    "items": {"type": "string"}
                },
                "contacts": {
                    "type": "array",
                    "items": {"$ref": "#/definitions/Contact"}
                },
                "compromises": {
                    "type": "array",
                    "items": {"type": "string"}
                },
                "affiliations": {
                    "type": "array",
                    "items": {"$ref": "#/definitions/Affiliation"}
                },
                "reputations": {
                    "type": "array",
                    "items": {"$ref": "#/definitions/Reputation"}
                },
                "artha": {"$ref": "#/definitions/Artha"},
                "spells": {
                    "type": "array",
                    "items": {"$ref": "#/definitions/Spell"}
                },
                "angry_spirits": {
                    "type": "array",
                    "items": {"$ref": "#/definitions/Spirit"}
                },
                "beliefs": {
                    "type": "array",
                    "items": {"type": "string"}
                },
                "instincts": {
                    "type": "array",
                    "items": {"type": "string"}
                },
                "notes": {
                    "type": "array",
                    "items": {"type": "string"}
                },
                "practice": {
                    "type": "array",
                    "items": {"$ref": "#/definitions/Practice"}
                },
                "mortality_modifier": {"type": "integer", "default": 0},
                "loans": {
                    "type": "array",
                    "items": {"type": "string"}
                },
                "debts": {
                    "type": "array",
                    "items": {"type": "string"}
                }
            },
            "required":  ["version", "owner", "profile", "lifepaths", "progressables", "belly", "traits", "beliefs", "instincts"]
        }
    },
    "$ref": "#/definitions/Character"
}