import unittest

from random import Random
from os import environ

environ['SEED'] = '42'

from burning_wheel_bot.funcs import dice_eval, inner_roll, choose, rand


class TestFunctions(unittest.TestCase):
    def setUp(self):
        rand.seed(int(environ['SEED']))
        self.rand = Random(int(environ['SEED']))

    def test_dice_eval(self):
        self.assertEqual(1, dice_eval('1'))
        self.assertEqual(4, dice_eval('3 + 1'))
        self.assertEqual(8, dice_eval('2 * (3+1)'))
        with self.assertRaises(ValueError):
            dice_eval('2 * (3+1')
        with self.assertRaises(ValueError):
            dice_eval('drop bobby tables')

    def test_inner_roll(self):
        self.assertEqual(('', [self.rand.randint(1, 6)],  6, 0, ''),
                         inner_roll(' 1d6'))
        self.assertEqual(('', [self.rand.randint(1, 20), self.rand.randint(1, 20)], 20, 0, 'test'),
                         inner_roll(' 2d20 test'))
        self.assertEqual(('', [100], None, 0, ''),
                         inner_roll(' (10*10)'))
        self.assertEqual(('', [100], None, 0, 'test'),
                         inner_roll(' (10*10) test'))
        self.assertEqual(('B', [self.rand.randint(1, 6), self.rand.randint(1, 6)], 6, 0, ''),
                         inner_roll(' B2'))
        self.assertEqual(('B', [self.rand.randint(1, 6), self.rand.randint(1, 6)], 6, 0, ''),
                         inner_roll(' B(2+1-1)'))
        self.assertEqual(('', [self.rand.randint(1, 20)], 20, -1, ''),
                         inner_roll(' 1d20 -1'))

        with self.assertRaises(ValueError):
            inner_roll('')
        with self.assertRaises(ValueError):
            inner_roll(' drop bobby tables')
        with self.assertRaises(ValueError):
            inner_roll(' d6')
        with self.assertRaises(ValueError):
            inner_roll(' P9')
        with self.assertRaises(ValueError):
            inner_roll(' (10*10)d')

    def test_choose(self):
        self.assertEqual(', '.join(self.rand.sample(['one'], 1)),
                         choose('!choose one'))
        self.assertEqual(', '.join(self.rand.sample(['one', 'two'], 1)),
                         choose('!choose one,two'))
        self.assertEqual(', '.join(self.rand.sample(['one', 'two'], 1)),
                         choose('!choose 1 one,two'))
        self.assertEqual(', '.join(self.rand.sample(['one', 'two'], 1)),
                         choose('!choose -r one,two'))
        self.assertEqual(', '.join(self.rand.sample(['one', 'two', 'three'], 2)),
                         choose('!choose 2 one,two,three'))
        self.assertEqual(', '.join(self.rand.choices(['one', 'two', 'three'], k=4)),
                         choose('!choose 4 -r one,two,three'))
