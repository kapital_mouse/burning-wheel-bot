from functools import partial
from operator import attrgetter

import requests
# from google.cloud import datastore

# from burning_wheel_bot.funcs import datastore_client, loop
from burning_wheel_bot.funcs.character import CHARACTER, cs_models, get_character, compute_ptgs
from burning_wheel_bot.utils import grouper

from collections import namedtuple

STATS = namedtuple('Stats', 'will perception agility speed power forte health reflexes steel resources circles hesitation')
PROGRESSABLE = namedtuple('Progressable', 'shade exponent injuries_affect')

injury_modifier = {
    'superficial': 0,
    'light': 1,
    'middling': 2,
    'severe': 3,
    'traumatic': 4,
    'mortal': 5
}


cs_template = \
"""
```
== Stats ==

Will:  {stats.will:>5} / Aglty: {stats.agility:>5} /  Power: {stats.power:>5}
Perc:  {stats.perception:>5} / Speed: {stats.speed:>5} /  Forte: {stats.forte:>5}

== Attributes ==

Helth: {stats.health:>5} / {ea[name]:<5}: {ea[display]:>5} / Rsrcs: {stats.resources:>5}
Reflx: {stats.reflexes:>5} /              /  Circle: {stats.circles:>5}
Steel: {stats.steel:>5}  / Hesit: {stats.hesitation:>5}   

PTGS: {ptgs!r} 
Injuries: {injuries}

== Skills ==

{skills}

== Artha ==
Fate:  {artha.fate:>5d} / Prsna: {artha.persona:>5d} / Deeds:  {artha.deeds:>5d}  

```
"""


def _fmt_progressable(injured, v):
    return ('{}{:d}' if not injured or not v.injuries_affect else '({}{})').format(
        v.shade, v.exponent if not injured or not v.injuries_affect else max(0, v.exponent - injured))


def _reflexes(*args):
    shade = 'B' if all(s.shade == 'B' for s in args) else 'G' if any(s.shade == 'G' for s in args) else 'W'
    exponent = sum(s.exponent + (0 if s.shade == shade else 2 if s.shade == 'G' else 1 if shade == 'G' else 3)
                   for s in args)
    return PROGRESSABLE(shade, exponent // len(args), True)


def cs(message):
    ch = get_character(message.channel.server.id, owner=message.author.id)

    injured = sum([0] + [injury_modifier[i.category] for i in ch.injuries])
    fmt = partial(_fmt_progressable, injured)

    try:
        ea = next(v for v in ch.progressables if v.category == 'emotional')
        ea = {'name': ea.name, 'display': fmt(ea)}
    except StopIteration:
        ea = {'name': '', 'display': ''}

    stats = {v.name: v for v in ch.progressables if v.category in {'stat', 'attribute'}}
    stats['reflexes'] = _reflexes(stats['perception'], stats['agility'], stats['speed'])
    stats['hesitation'] = PROGRESSABLE('', 10 - stats['will'].exponent, False)
    stats_fmt = STATS(**{str(k): fmt(v) for k, v in stats.items()})

    skills = '\n'.join(
        ' / '.join('{name:<5}: {exponent:>5}'.format(
            name=v.name.capitalize(),
            exponent=fmt(v)) if v else ' ' * 12
                   for v in g)
        for g in grouper(sorted([v for v in ch.progressables if v.category == 'skill'], key=attrgetter('name')), 2)
    )

    return cs_template.format(
        stats=stats_fmt,
        ea=ea,
        ptgs=compute_ptgs(stats['forte'].shade, stats['forte'].exponent, stats['power'].shade, stats['power'].exponent,
                          ch.mortality_modifier),
        injuries=', '.join(i.category.capitalize() for i in ch.injuries),
        skills=skills,
        artha=ch.artha
    )


def csfull(message):
    pass


async def csload(message):
    # if we are getting DM'd or trying to be uploaded by a non-GM
    if not hasattr(message.author, 'roles') or not any(r.name == 'GM' for r in message.author.roles):
        return "Sorry, you don't have permissions to do that"

    if not len(message.attachments):
        return "You need to upload an attachment"

    if not message.attachments[0]['url'].endswith('.json'):
        return "Must upload a JSON file"

    resp = await loop.run_in_executor(None, requests.get, message.attachments[0]['url'])
    if resp.ok:
        if resp.raw.headers.get('Content-Type') != 'application/json':
            return "Must upload a JSON file"

        ch = cs_models.Character.from_json(resp.content.decode('utf8'))
        await loop.run_in_executor(None, store_character, message.channel.server.id, ch)
        return "success -- added {} for <@{}>".format(ch.profile.name, ch.owner)


def store_character(server, ch):
    with datastore_client.transaction():
        incomplete_key = datastore_client.key(CHARACTER)

        task = datastore.Entity(key=incomplete_key, exclude_from_indexes=('json',))

        task.update({'server': server, 'owner': ch.owner, 'name': ch.profile.name,
                     'version': ch.version, 'json': ch.serialize()})
        datastore_client.put(task)
