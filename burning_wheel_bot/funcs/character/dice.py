import re

from burning_wheel_bot.funcs import roll, xroll
from burning_wheel_bot.funcs.character import get_character

csroll_p = re.compile('!csx?roll (?P<skill>\s*(".{3,}"|\S{3,}))(?P<mod>\s+[+-]\d+)?\s*(?P<message>.*)?')


def csroll(message):
    m = csroll_p.match(message.content)
    if not m:
        return 'Malformed CSRoll Expression: `!csroll <skill> <mod>? <message>?`'

    skill = m.group('skill').lower().strip().strip('"')
    mod = int((m.group('mod') or '').lower().strip() or '0')

    try:
        ch = get_character(message.channel.server.id, owner=message.author.id)
    except ValueError:
        return 'Could not find a character for you to roll with...'

    cand = [p for p in ch.progressables if p.name.startswith(skill)]
    if len(cand) == 1:
        to_roll = cand[0]
        fn = roll if message.content.startswith('!csroll') and not to_roll.open_ended else xroll
        return fn(' {0}{1} {2} ({0}{3}) {4}'.format(to_roll.shade or 'B', to_roll.exponent + mod,
                                                    to_roll.name.capitalize(),
                                                    to_roll.exponent, m.group('message') or ''))

    elif cand:
        return 'found multiple candidates: {}'.format(','.join(p.name for p in cand))
    else:
        return 'could not find an appropriate skill to roll!'
