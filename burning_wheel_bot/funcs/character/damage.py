from collections import namedtuple
from math import ceil, floor
import re

from burning_wheel_bot.funcs.dice import dice_eval


# !ptgs forte power modifier
ptgs_p = re.compile('!ptgs\s+(?P<forte>[bBgGwW]\d+)\s+(?P<power>[bBgGwW]\d+)(?P<mod>\s+[+-]\d+)?(?P<message>.*)')

# !ims power
ims_p = re.compile('!ims (?P<shade>[bBgGwW])?(?P<exponent>\d+|\([ \d+\-*/()]+\))(?P<message>.*)')

PTGS = namedtuple('PTGS', 'superficial light middling severe traumatic mortal')
IMS = namedtuple('PTGS', 'incidental mark superb')


def _score_repr(score):
    return '{}{}'.format('B' if score <= 16 else 'G' if score <= 33 else 'W',
                         max(1, score) if score <= 16 else score - 16 if score <= 32 else min(16, score - 32))


def ims_repr(self):
    return '{} / {} / {}'.format(*list(map(_score_repr, self)))


def ptgs_repr(self):
    return '{} / {} / {} / {} / {} / {}'.format(
        *list(map(_score_repr, self))
    )


PTGS.__repr__ = ptgs_repr
IMS.__repr__ = ims_repr


def compute_ptgs(forte_shade, forte, power_shade, power, mod):
    shades = [forte_shade, power_shade]
    has_b, has_g, has_w = 'B' in shades, 'G' in shades, 'W' in shades

    # mundane PTGS (just about everything). Page 98 BWG.
    if has_b:
        mortal = (forte + power + (2 if has_g else 3 if has_w else 0)) // 2 + mod + 6
        gap = int(ceil((forte + (2 if forte_shade == 'G' else 3 if forte_shade == 'W' else 0)) / 2))

        # this are almost certainly overestimate mortality; will be corrected later on
        superficial = (forte + (2 if forte_shade == 'G' else 3 if forte_shade == 'W' else 0)) // 2 + 1
        light = superficial + gap
        middling = light + gap
        severe = middling + gap
        traumatic = severe + gap
    elif has_g:
        mortal = (forte + power + has_w) // 2 + mod + 6 + 16

        # Monster Burner, page 369
        traumatic = mortal - 1
        severe = traumatic - 1
        middling = severe - 2
        light = (forte + (1 if forte_shade == 'W' else 0)) * 2
        superficial = forte + (1 if forte_shade == 'W' else 0)
    else:
        mortal = (forte + power) // 2 + mod + 6 + 32

        # Monster Burner, page 370
        traumatic = mortal - 1
        severe = traumatic - 1
        middling = severe - 2
        light = forte * 4
        superficial = forte * 2

    # correct for errors:
    #   1: clamp between B6..W16 for mortal (and -1/-1 for trauma, -2/-2 for severe, ...)
    #   2: make sure that trauma is always smaller than mortal, etc
    mortal = max(6, min(48, mortal))
    traumatic = max(5, min(mortal - 1, traumatic))
    severe = max(4, min(traumatic - 1, severe))
    middling = max(3, min(severe - 1, middling))
    light = max(2, min(middling - 1, light))
    superficial = max(1, min(light - 1, superficial))

    return PTGS(superficial, light, middling, severe, traumatic, mortal)


def ptgs(message):
    m = ptgs_p.match(message)
    if not m:
        return 'Malformed PTGS Expression: `!ptgs <forte> <power> <modifier>`'

    forte_shade, forte = m.group('forte')[0].upper(), int(m.group('forte')[1:])
    power_shade, power = m.group('power')[0].upper(), int(m.group('power')[1:])

    mod = 0
    if m.group('mod') and m.group('mod').strip():
        mods = m.group('mod').strip()
        mod = int(mods[1:]) * (1 if mods[0] == '+' else -1)

    return '{}: {}'.format(
        m.group('message') or 'PTGS',
        repr(compute_ptgs(forte_shade, forte, power_shade, power, mod)))


def inner_ims(shade, exponent):
    i = ceil(exponent / 2)
    m = exponent
    s = floor(exponent * 1.5)

    if shade == 'G':
        i, m, s = i + 16, m + 16, s + 16
    elif shade == 'W':
        i, m, s = i + 32, m + 32, s + 32

    return IMS(i, m, s)


def ims(message):
    m = ims_p.match(message)
    if not m:
        return 'Malformed IMS Expression: `!ims <shade><power>'

    shade = (m.group('shade') or 'B').upper()
    exponent = dice_eval(m.group('exponent'))

    return '{}: {}'.format(m.group('message') or 'IMS', repr(inner_ims(shade, exponent)))
