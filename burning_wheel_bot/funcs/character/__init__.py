import python_jsonschema_objects as pjs

cs_builder = pjs.ObjectBuilder('data/cs_schema.json')
cs_models = cs_builder.build_classes()

CHARACTER = 'Character'

from burning_wheel_bot.funcs.character.utils import *
from burning_wheel_bot.funcs.character.damage import ptgs, ims, compute_ptgs
from burning_wheel_bot.funcs.character.manage import cs, csfull, csload
from burning_wheel_bot.funcs.character.dice import csroll