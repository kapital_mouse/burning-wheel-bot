from functools import lru_cache

# from burning_wheel_bot.funcs import datastore_client
from burning_wheel_bot.funcs.character import CHARACTER, cs_models


@lru_cache(maxsize=64)
def get_character(server, owner):
    q = datastore_client.query(kind=CHARACTER)
    q.add_filter('server', '=', server)
    q.add_filter('owner', '=', owner)
    q.order = ['-version']

    res = list(q.fetch(limit=1))
    if len(res) == 0:
        raise ValueError("no valid character!")
    return cs_models.Character.from_json(res[0]['json'])