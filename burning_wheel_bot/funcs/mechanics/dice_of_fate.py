import re

from burning_wheel_bot.funcs import rand

IMS_I = 'Incidental'
IMS_M = 'Mark'
IMS_S = 'Superb'

DOF_YES = 'Yes!'
DOF_NO = 'No'

dof_types = {
    'bow': {1: IMS_I, 2: IMS_I, 3: IMS_M, 4: IMS_M, 5: IMS_S, 6: IMS_S},
    'gun': {1: IMS_I, 2: IMS_I, 3: IMS_I, 4: IMS_I, 5: IMS_M, 6: IMS_S},
    'magic': {1: IMS_I, 2: IMS_I, 3: IMS_M, 4: IMS_M, 5: IMS_S, 6: IMS_S},
    'improvised': {1: IMS_I, 2: IMS_I, 3: IMS_I, 4: IMS_I, 5: IMS_M, 6: IMS_S},
    'thrown': {1: IMS_I, 2: IMS_I, 3: IMS_M, 4: IMS_M, 5: IMS_M, 6: IMS_S},
    'bomb': {1: IMS_I, 2: IMS_I, 3: IMS_M, 4: IMS_M, 5: IMS_S, 6: IMS_S},
    'rock': {1: IMS_I, 2: IMS_I, 3: IMS_I, 4: IMS_M, 5: IMS_M, 6: IMS_S},
    'dof': {1: DOF_YES, 2: DOF_NO, 3: DOF_NO, 4: DOF_NO, 5: DOF_NO, 6: DOF_NO}
}


# !dof [bow|gun|magic|improvised|thrown|bomb|rock|dof] <result mod> <message>?
dof_p = re.compile(r'!dof\s*(?P<type>(?<=\s){})?(?P<mod>(?<=\s)[\-+]\s*\d+)?\s*(?P<message>.*)'.format('|'.join(dof_types.keys())))


def dof(message):
    m = dof_p.match(message)
    if not m:
        return 'Malformed dof Expression: `!dof [{}] <result mod> <message>?`'.format('|'.join(dof_types.keys()))

    type_ = m.group('type') or 'dof'
    res = rand.randint(1, 6) + (int(m.group('mod') or 0) if type_ != 'dof' else 0)
    return '{}: **{}** ({})'.format(m.group('message') or type_, dof_types[type_][res], res)
