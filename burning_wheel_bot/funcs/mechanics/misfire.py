import re
from burning_wheel_bot.funcs import rand
from burning_wheel_bot.strings import MISFIRE_WHEEL_TURN

# !misfire <element> <impetus> <origin> <duration> <area>
misfire_p = re.compile(r'!misfire\s+(?P<element>\S+)\s+(?P<impetus>\S+)\s+(?P<origin>\S+)\s+(?P<duration>\S+)\s+(?P<area>\S+)(?P<message>.*)')


wheel_of_magic = {
    'element': ['arcana', 'heaven', 'white', 'fire', 'air', 'earth', 'water', 'anima'],
    'impetus': ['enhance', 'create', 'destroy', 'tax', 'transmute', 'control', 'influence'],
    'origin': ['personal', 'sight', 'presence'],
    'duration': ['permanent', 'sustained', 'instantaneous', 'elapsed'],
    'area': ['single target', 'caster',
             'double measured area', 'measured area', 'half measured area',
             'double natural effect', 'natural effect', 'half natural effect',
             'double presence', 'presence', 'half presence']
}

unwanted_summoning = {
    2: 'Deity',
    3: 'Major Corporeal Spirit',
    4: 'Sanctified Dead',
    5: 'Sanctified Dead',
    6: 'Restless Dead',
    7: 'Restless Dead',
    8: 'Minor Corporeal Spirit',
    9: 'Minor Corporeal Spirit',
    10: 'Corporal Spirit',
    11: 'Minor Deity',
    12: 'Chief Deity'
}

elapsed_time = {
    2: 'Years',
    3-4: 'Actions',
    5-6: 'Seconds',
    7-8: 'Exchanges',
    9: 'Minutes',
    10: 'Hours',
    11: 'Days',
    12: 'Months'
}

half_measured_area = {
    1: 'Half a Pace',
    2: '5 Paces',
    3: '50 Paces',
    4: 'Half a Mile',
    5: '5 Miles',
    6: '10 Miles'
}

measured_area = {
    1: 'Paces',
    2: '10s of Paces',
    3: '100s of Paces',
    4: 'Miles',
    5: '10s of Miles',
    6: '100s of Miles'
}

double_measured_area = {
    1: 'Meters',
    2: '20s of Paces',
    3: '200s of Paces',
    4: '2 Miles',
    5: '20s of Miles',
    6: '200s of Miles'
}


def lookup(q, opts, context):
    matches = [o.startswith(q) for o in opts]
    if sum(matches) == 1:
        return next(o for o, m in zip(opts, matches) if m)
    elif any(matches):
        raise ValueError('Multiple {} Matches! {} => {}'.format(
            context, q, ', '.join(o.capitalize() for o, m in zip(opts, matches) if m)))
    else:
        raise ValueError('No Such {} Match: {}'.format(context, q))


def cycle_access(arr, ndx):
    return arr[ndx % (len(arr) if ndx >= 0 else -len(arr))]


def misfire(message):
    m = misfire_p.match(message)
    if not m:
        return 'Malformed Misfire Expression: `!misfire <element> <impetus> <origin> <duration> <area>`'

    try:
        cast = {k: lookup(g.lower(), wheel_of_magic[k], k.capitalize()) for k, g in m.groupdict().items()
                if k in wheel_of_magic}
    except ValueError as e:
        return str(e)

    dof = rand.randint(1, 6)
    if dof == 1:
        res = rand.randint(1, 6) + rand.randint(1, 6)
        return '{message}**Unwanted Summoning: {result}**'.format(
            message='{}: '.format(m.group('message')) if m.group('message') else '', result=unwanted_summoning[res])
    elif dof == 2:
        direction = rand.choice([-1, 1])
        magnitude = rand.randint(1, 6)
        change = direction * magnitude

        new_cast = {k.capitalize(): cycle_access(wheel_of_magic[k], wheel_of_magic[k].index(c) + change).capitalize()
                    for k, c in cast.items() if k in wheel_of_magic}

        new_duration, new_area = new_cast['Duration'], new_cast['Area']
        if 'elapsed' in new_duration.lower():
            new_cast['Duration'] = '{}: {}'.format(new_duration,
                                                   elapsed_time[rand.randint(1, 6) + rand.randint(1, 6)])
        if 'measured' in new_area.lower():
            effect = double_measured_area if new_area.startswith('Double') else half_measured_area \
                if new_area.startswith('Half') else measured_area
            new_cast['Area'] = '{}: {}'.format(new_area, effect[rand.randint(1, 6)])

        return MISFIRE_WHEEL_TURN.format(
            message='{}: '.format(m.group('message')) if m.group('message') else '', magnitude=magnitude,
            direction='Clockwise' if direction == 1 else 'Counter-Clockwise', **new_cast)
    else:
        return '{message}**Harmless Dissipation!**'.format(
            message='{}: '.format(m.group('message')) if m.group('message') else '')
