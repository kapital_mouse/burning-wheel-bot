import ast
import operator
import re

from burning_wheel_bot.funcs import rand

# !roll <shade>? <number of dice> d? <dice sides>? <result mod>? <message>?
roll_p = re.compile(r'\s+([bgwBGW]?)(\d+|\([ \d+\-*/()]+\))(d?)(\d*)\s*([+\-]\s*(?:\d+|\([ \d+\-*/()]\)))?\s*(.*)')

# !multiroll <number> <shade>? <number of dice> d? <dice sides>? <message>?
multiroll_p = re.compile(r'!multiroll\s+(\d+|\([ \d+\-*/()]+\))' + roll_p.pattern)

# !choose <number> <-r> <options>
choose_p = re.compile(r'!choose\s*((?<=\s)\d*)?\s*((?<=\s)-r)?\s*((?<=\s)[\w\s,\']+)')

shades = {
    'b': 4, 'B': 4,
    'G': 3, 'g': 3,
    'W': 2, 'w': 2
}


def dice_eval(s):
    """
    modified from ast.literal_eval except handling multiplications and divisions which aren't supported by default
    :param s: a string to evaluate. Such as "3 + 2" or "8 * (3-1)"
    :type s: str
    """

    try:
        node = ast.parse(s, mode='eval')
    except SyntaxError:
        raise ValueError('malformed node or string: {}'.format(s))

    ops = {
        ast.Add: operator.add,
        ast.Sub: operator.sub,
        ast.Mult: operator.mul,
        ast.Div: operator.ifloordiv
    }

    def _convert(node):
        # parentheses
        if isinstance(node, ast.Expression):
            return _convert(node.body)
        # literal numbers
        elif isinstance(node, ast.Num):
            return node.n
        # arithmetic operators
        elif isinstance(node, ast.BinOp):
            # rather than only handling add/sub, recur and fail fast later
            if type(node.op) not in ops:
                raise ValueError('unknown Binary Operation: '.format(node.op))
            return ops[type(node.op)](_convert(node.left), _convert(node.right))
        else:
            raise ValueError('malformed node or string: '.format(s))
    try:
        return _convert(node.body)
    except ZeroDivisionError:
        raise ValueError('dividing by zero')


def inner_roll(message):
    """
    Parse and validate user messages requesting dice rolls
    Raises `ValueError` when the message is malformed

    :return: dice shade, rolls, sides, mod, message
    :rtype: (str | None, list[int],  int | None, int, str | None)
    """
    m = roll_p.match(message)
    if not m:
        raise ValueError('Malformed Roll Expression: `!roll NxS <mod> <message>`')
    else:
        shade = m.group(1)
        if shade and shade not in shades:
            raise ValueError('Unknown Shade')

        # count is either a simple \d+ group or we have to do arithmetic
        try:
            count = dice_eval(m.group(2)) if '(' in m.group(2) else int(m.group(2))
        except ValueError:
            raise ValueError('Malformed `N` Expression: `!roll NxS <mod> <message>`')

        # if we have a modifier, make sure it is valid (do the arithmetic compared to 0)
        if m.group(5):
            try:
                mod = dice_eval('0 {}'.format(m.group(5)))
            except ValueError:
                raise ValueError('Malformed Result Modifier: `!roll NxS <mod> <message>`')
        else:
            mod = 0

        # if we have d but no sides, that's an error (it's not a number calculation)
        if m.group(3) and not m.group(4):
            raise ValueError('Malformed `N` Expression: `!roll NxS <mod> <message>`')

        # if no shade and no sides, then just doing a number calculation
        if not shade and not m.group(4):
            return shade, [count], None, mod, m.group(6)

        sides = 6 if shade else int(m.group(4))
        if count <= 0 or count > 100 or sides <= 0:
            raise ValueError('Invalid number of dice or sides')
        return shade, [rand.randint(1, sides) for _ in range(count)], sides, mod, m.group(6)


def fmt_roll(rolls, bolded, sides):
    return ', '.join(('**{}**'.format(v) if v in bolded else str(v) for v in rolls) if sides else map(str, rolls))


def roll(message):
    """
    Create a nicely formatted roll message using `inner_roll` as a driver
    """
    try:
        shade, rolls, sides, mod, msg = inner_roll(message)
        bolded = {1, sides}
        # assume black shades for 6 sided dice
        if not shade and sides == 6:
            shade = 'B'

        ret = '**{}:** {}\n**Total:** {}'.format(
            msg or 'Result',
            fmt_roll(rolls, bolded, sides),
            sum(rolls) + mod
        )
        if shade:
            ret += '\n**Successes**: {}\n**Explosions**: {}'.format(
                sum(v >= shades[shade] for v in rolls), sum(v == sides for v in rolls))
        return ret
    except ValueError as e:
        return str(e)


def xroll(message):
    """
    Create a nicely formatted roll that auto-explodes using `inner_roll` as a driver
    """
    try:
        shade, rolls, sides, mod, msg = inner_roll(message)
        results = [rolls]
        # as long as we have a valid `sides` definition and we get exploding dice, keep rolling
        # also make sure we aren't rolling one sided dice over and over (sides must be > 1)
        while sides and sides > 1 and any(r == sides for r in rolls):
            rolls = [rand.randint(1, sides) for _ in range(sum(r == sides for r in rolls))]
            results.append(rolls)

        bolded = {1, sides}
        # assume black shades for 6 sided dice
        if not shade and sides == 6:
            shade = 'B'

        ret = '**{}:** {}'.format(
            msg or 'Result',
            ' / '.join('({})'.format(fmt_roll(r, bolded, sides)) for r in results)
        )
        if shade:
            ret += '\n**Successes**: {} = `{}`\n**Explosions**: {}'.format(
                ' + '.join(str(sum(v >= shades[shade] for v in r)) for r in results),
                sum(v >= shades[shade] for r in results for v in r),
                sum(v == sides for r in results for v in r))
        return ret
    except ValueError as e:
        return str(e)


def multiroll(message):
    """
    Create a nicely formatted multiroll message using `inner_roll` as a driver
    """
    m = multiroll_p.match(message)
    if not m:
        return 'Malformed Multiroll Expression: `!multiroll N <roll expression>`'
    try:
        n = dice_eval(m.group(1)) if '(' in m.group(1) else int(m.group(1))
    except ValueError:
        return 'Malformed `N` Expression: `!multiroll N <roll expression>`'
    if n <= 0 or n > 10:
        return 'N must be between 1 and 10'

    results = []
    for _ in range(n):
        try:
            shade, rolls, sides, mod, msg = inner_roll(message[m.end(1):])
        except ValueError as e:
            return 'Malformed inner roll expression: {}'.format(e)
        results.append(rolls)

    bolded = {1, sides}
    return '\n'.join(
        (['{}'.format(msg)] if msg else []) +
        ['Rolling **{}** iterations of ({}d{}{})...'.format(
            n, len(results[0]), sides, '+{}'.format(mod) if mod > 0 else mod if mod else '')] +
        ['({}) = `{}`'.format(fmt_roll(rolls, bolded, sides), sum(rolls) + mod) for rolls in results] +
        ['{} total.'.format(sum(sum(r) + mod for r in results))]
    )


def choose(message):
    m = choose_p.match(message)
    if not m:
        return 'Malformed Choose Expression: `!choose <N> <-r> <choices>`'

    choices = m.group(3).split(',')
    count = int(m.group(1).strip()) if m.group(1).strip() else 1
    with_replace = m.group(2) is not None

    if count < 1 or (not with_replace and count > len(choices)):
        return 'Must choose at least one and no more than N (without replacement)'

    return ', '.join(rand.choices(choices, k=count) if with_replace else rand.sample(choices, count))
