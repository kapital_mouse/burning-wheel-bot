import re

from burning_wheel_bot.funcs import rand
from burning_wheel_bot.funcs.dice import dice_eval

# !odds <shade><number of dice> <obstacle>
odds_p = re.compile(r'\s+([bgwBGW]?)(\d+|\([ \d+\-*/()]+\))\s+(\d+|\([ \d+\-*/()]+\))')


def sim_roll(shade, n, explode=True):
    if not n:
        return 0
    rolls = [rand.randint(1, 6) for _ in range(n)]
    return sum(r >= shade for r in rolls) + (sim_roll(shade, sum(r == 6 for r in rolls)) if explode else 0)


def odds(message):
    m = odds_p.match(message)
    if not m:
        return 'Malformed Odds Expression: `!odds <shade><dice> <obstacle>'

    shade, dice, obs = m.groups()
    try:
        dice = dice_eval(dice)
        obs = dice_eval(obs)
    except ValueError as e:
        return str(e)

    r = sum(sim_roll(4 if shade in 'bB' else 3 if shade in 'gG' else 2, dice) >= obs for _ in range(100000)) / 100000

    return '**{}{} >= {}**: {:.1%}'.format(shade, dice, obs, r)