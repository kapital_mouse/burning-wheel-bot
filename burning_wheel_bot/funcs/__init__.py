import asyncio
from os import environ
from random import Random
# from google.cloud import datastore

rand = Random(int(environ['SEED']) if 'SEED' in environ else None)
# datastore_client = datastore.Client()
loop = asyncio.get_event_loop()

from burning_wheel_bot.funcs.dice import roll, multiroll, xroll, choose
from burning_wheel_bot.funcs.mechanics.misfire import misfire
from burning_wheel_bot.funcs.mechanics.dice_of_fate import dof
from burning_wheel_bot.funcs.character import cs, csfull, csload, csroll, ptgs, ims
from burning_wheel_bot.funcs.odds import odds
