HELP_DICT = {
    '!roll': \
"""`!roll 1d20` -- roll a single 20-sided die
`!roll 2d20 +5` -- roll two 20-sided die, adding 5 to entire result
`!roll 4d6 pick the lock` -- roll 4 6-sided dice, with a message of "pick the lock"
`!roll (3+3)d10` -- roll (3+3) => 6 10-sided dice. All basic arithmetic is supported. note that division is truncated
`!roll B4` -- Burning Wheel notation: roll 4 mundane dice
`!roll (10*(10+1))` -- calculator mode: will return the result `110` """,
    '!multiroll': """`!multiroll 5 4d20` -- roll 4 20-sided dice 5 times""",
    '!choose': \
"""!choose red,blue,green -- choose from one of three things
!choose 2 -r 1,2,3,4,5,6 -- simulate rolling 2 6-sided dice really inefficiently""",
    '!misfire': \
"""
!misfire <element> <impetus> <origin> <duration> <area>
!misfire <spell name>

For both interpreting the results of the spell and specifying the original spell, see the below guide

Elements: The element powering the spell
```
Air is the element of the wind
Anima is the element of the bodies and minds of creatures
Arcana is the sphere of Arcane Power -- magic itself
Earth is stone, wood, dirt
Fire is heat and flame
Heaven invokes the sphere of light
Water encompasses rivers, seas, ponds and streams
White is the dangerous sphere of heavenly fire: lightning and other terrible weather
```

Impetus: The purpose of the spell
```
Control dominates and commandeers the element, forcing it into unlikely or impossible positions or situations
Create involves fabrication of materials or substance
Destroy is the dark art to destroy living creatures
Enhance fortifies and strengthens to an otherworldly degree
Influence subtly alters existing creatures and situations
Tax is another black art design to drain or weaken the strength of its target element
```

Origin: Where the spell starts
```
Personal -- from the caster
Presence -- anywhere the speaker could speak and expect to be heard
Sight -- if the caster sees it, the caster can affect it
```

Duration: How long does the spell's non-lingering effects last?
```
Instantaneous -- the spell flickers into being but for a moment and gone. Any effects it has on the environment remain but will fade over time
Permanent -- the spell will never fade without magical interference 
Elapsed Time -- some pre-determined amount of time
Sustained -- as long as the caster wills it and devotes magic to sustaining it
```

Area:
```
Caster -- the one who cast the spell
Single Target -- someone other than the caster
Measured Area -- a predetermined group of targets or area
Natural Effect -- the amount of area the element would normally fill up on its own
Presence -- around the caster
```
""",
    '!dof': 'roll a dice of fate for ranged weapons or for strange happenings',
    '!ptgs': \
"""calculate the PTGS of a character. <forte> <power> <modifier> where <modifier> modifies the final `Mortal Wound` stat.'
```!ptgs <forte> <power> <modifier>```
`Superficial` / `Light` / `Middling` / `Severe` / `Traumatic` / `Mortal`
"""
}

HELP = """
This is a dice and RP helper bot written by kapital-mouse

**Basic Functionality**

`!roll <N>d<S>` -- roll some dice. 
`!xroll <N>d<S>`-- auto-explodes dice. Note that `!xroll` does not support modifiers    
`!multiroll` -- do multiple !roll commands in quick succession
`!choose` -- choose from a list of comma-delimited options (optionally with replacement, or more than one)
    
**Burning Wheel Commands**
    
`!dof` -- Dice of Fate
`!ptgs` -- Compute the PTGS of a character
`!misfire` -- compute the result of a failed magic test 

**Bot Management Commands**    

`!ping` -- make sure the bots alive
`!help` -- this message
`!help <command>` -- see more information about a command
"""

MISFIRE_WHEEL_TURN = """{message}**Garbled Transmission!**
_Wheel of Magic turn, turn, turn. Tell me the lesson I have learned!_
{direction} {magnitude}
Element: **{Element}**\nImpetus: **{Impetus}**\nOrigin: **{Origin}** 
Duration: **{Duration}**\nArea: **{Area}**"""