from python_jsonschema_objects.classbuilder import LiteralValue


LiteralValue.__format__ = lambda self, format_spec: format(self._value, format_spec)
LiteralValue.__getattr__ = lambda self, name: getattr(self._value, name)

LiteralValue.__add__ = lambda self, other: self._value + other if not isinstance(other, LiteralValue) else self._value + other._value
LiteralValue.__sub__ = lambda self, other: self._value - other if not isinstance(other, LiteralValue) else self._value - other._value
LiteralValue.__mul__ = lambda self, other: self._value * other if not isinstance(other, LiteralValue) else self._value * other._value
LiteralValue.__matmul__ = lambda self, other: self._value @ other if not isinstance(other, LiteralValue) else self._value @ other._value
LiteralValue.__truediv__ = lambda self, other: self._value / other if not isinstance(other, LiteralValue) else self._value / other._value
LiteralValue.__floordiv__ = lambda self, other: self._value // other if not isinstance(other, LiteralValue) else self._value // other._value
LiteralValue.__mod__ = lambda self, other: self._value % other if not isinstance(other, LiteralValue) else self._value % other._value
LiteralValue.__divmod__ = lambda self, other: divmod(self._value, other) if not isinstance(other, LiteralValue) else divmod(self._value, other._value)
LiteralValue.__pow__ = lambda self, other, modulo=None: pow(self._value, other, modulo) if not isinstance(other, LiteralValue) else pow(self._value, other._value, modulo)
LiteralValue.__lshift__ = lambda self, other: self._value << other if not isinstance(other, LiteralValue) else self._value << other._value
LiteralValue.__rshift__ = lambda self, other: self._value >> other if not isinstance(other, LiteralValue) else self._value >> other._value
LiteralValue.__and__ = lambda self, other: self._value & other if not isinstance(other, LiteralValue) else self._value & other._value
LiteralValue.__xor__ = lambda self, other: self._value ^ other if not isinstance(other, LiteralValue) else self._value ^ other._value
LiteralValue.__or__ = lambda self, other: self._value | other if not isinstance(other, LiteralValue) else self._value | other._value


LiteralValue.__radd__ = lambda self, other: other + self._value if not isinstance(other, LiteralValue) else other._value + self._value
LiteralValue.__rsub__ = lambda self, other: other - self._value if not isinstance(other, LiteralValue) else other._value - self._value
LiteralValue.__rmul__ = lambda self, other: other * self._value if not isinstance(other, LiteralValue) else other._value * self._value
LiteralValue.__rmatmul__ = lambda self, other: other @ self._value if not isinstance(other, LiteralValue) else other._value @ self._value
LiteralValue.__rtruediv__ = lambda self, other: other / self._value if not isinstance(other, LiteralValue) else other._value / self._value
LiteralValue.__rfloordiv__ = lambda self, other: other // self._value if not isinstance(other, LiteralValue) else other._value // self._value
LiteralValue.__rmod__ = lambda self, other: other % self._value if not isinstance(other, LiteralValue) else other._value % self._value
LiteralValue.__rdivmod__ = lambda self, other: divmod(other, self._value) if not isinstance(other, LiteralValue) else divmod(other._value, self._value)
LiteralValue.__rpow__ = lambda self, other, modulo=None: pow(other, self._value, modulo) if not isinstance(other, LiteralValue) else pow(other._value, self._value, modulo)
LiteralValue.__rlshift__ = lambda self, other: other << self._value if not isinstance(other, LiteralValue) else other._value << self._value
LiteralValue.__rrshift__ = lambda self, other: other >> self._value if not isinstance(other, LiteralValue) else other._value >> self._value
LiteralValue.__rand__ = lambda self, other: other & self._value if not isinstance(other, LiteralValue) else other._value & self._value
LiteralValue.__rxor__ = lambda self, other: other ^ self._value if not isinstance(other, LiteralValue) else other._value ^ self._value._value
LiteralValue.__ror__ = lambda self, other: other | self._value if not isinstance(other, LiteralValue) else other._value | self._value


LiteralValue.__lt__ = lambda self, other: self._value < other if not isinstance(other, LiteralValue) else self._value < other._value
LiteralValue.__le__ = lambda self, other: self._value <= other if not isinstance(other, LiteralValue) else self._value <= other._value
LiteralValue.__eq__ = lambda self, other: self._value == other if not isinstance(other, LiteralValue) else self._value == other._value
LiteralValue.__gt__ = lambda self, other: self._value > other if not isinstance(other, LiteralValue) else self._value > other._value
LiteralValue.__ge__ = lambda self, other: self._value >= other if not isinstance(other, LiteralValue) else self._value >= other._value


def literalIAdd(self, other):
    self._value += other if not isinstance(other, LiteralValue) else other._value


def literalISub(self, other):
    self._value -= other if not isinstance(other, LiteralValue) else other._value


def literalIMul(self, other):
    self._value *= other if not isinstance(other, LiteralValue) else other._value


def literalIMatMul(self, other):
    self._value @= other if not isinstance(other, LiteralValue) else other._value


def literalITrueDiv(self, other):
    self._value /= other if not isinstance(other, LiteralValue) else other._value


def literalIFloorDiv(self, other):
    self._value //= other if not isinstance(other, LiteralValue) else other._value


def literalIMod(self, other):
    self._value %= other if not isinstance(other, LiteralValue) else other._value


def literalIPow(self, other, modulo=None):
    self._value = pow(self._value, other, modulo) if not isinstance(other, LiteralValue) else pow(self._value, other._value, modulo)


def literalILShift(self, other):
    self._value <<= other if not isinstance(other, LiteralValue) else other._value


def literalIRShift(self, other):
    self._value >>= other if not isinstance(other, LiteralValue) else other._value


def literalIAnd(self, other):
    self._value &= other if not isinstance(other, LiteralValue) else other._value


def literalIXor(self, other):
    self._value ^= other if not isinstance(other, LiteralValue) else other._value


def literalIOr(self, other):
    self._value |= other if not isinstance(other, LiteralValue) else other._value


LiteralValue.__iadd__ = literalIAdd
LiteralValue.__isub__ = literalISub
LiteralValue.__imul__ = literalIMul
LiteralValue.__imatmul__ = literalIMatMul
LiteralValue.__itruediv__ = literalITrueDiv
LiteralValue.__ifloordiv__ = literalIFloorDiv
LiteralValue.__imod__ = literalIMod
LiteralValue.__ipow__ = literalIPow
LiteralValue.__ilshift__ = literalILShift
LiteralValue.__irshift__ = literalIRShift
LiteralValue.__iand__ = literalIAnd
LiteralValue.__ixor__ = literalIXor
LiteralValue.__ior__ = literalIOr


LiteralValue.__neg__ = lambda self: -self._value
LiteralValue.__pos__ = lambda self: +self._value
LiteralValue.__abs__ = lambda self: abs(self._value)
LiteralValue.__invert__ = lambda self: ~self._value


LiteralValue.__complex__ = lambda self: complex(self._value)
LiteralValue.__round__ = lambda self, n=None: round(self._value, n)

LiteralValue.__index__ = lambda self: int(self._value)
