import discord
import os

import burning_wheel_bot.monkeypatch
from burning_wheel_bot.funcs import *
from burning_wheel_bot.strings import HELP, HELP_DICT

client = discord.Client()
bd_starboard: discord.Channel = None

starboard_id = '516683617901084673'


async def help(message):
    if message.content.strip() == '!help':
        await client.send_message(message.author, HELP)
        if not message.channel.is_private:
            await client.send_message(message.channel,
                                      'I have sent you a message to your DMs {0.author.mention}'.format(message))
    else:
        sent = False
        for command in message.content.strip().split(' ')[1:]:
            if command in HELP_DICT:
                await client.send_message(message.author, HELP_DICT[command])
                sent = True

        if not sent:
            await client.send_message(message.channel, 'Sorry, I don\'t have a HELP for ({})'.format(
                ', '.join(message.content.strip().split(' ')[1:])))
        elif not message.channel.is_private:
            await client.send_message(message.channel,
                                      'I have sent you a message to your DMs {0.author.mention}'.format(message))


@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

    global bd_starboard
    bd_starboard = client.get_channel(starboard_id)


async def get_already_logged():
    ret = []
    async for m in client.logs_from(bd_starboard, limit=100):
        ret.extend(e.get('footer', {}).get('text', '') for e in m.embeds)
    return set(ret)


@client.event
async def on_reaction_add(reaction, user):
    if not reaction.message.channel.is_private and \
            isinstance(reaction.emoji, str) \
            and reaction.emoji == '\u2b50' and reaction.count >= 3 \
            and reaction.message.channel.id != starboard_id \
            and reaction.message.id not in await get_already_logged():

        embed = discord.Embed(title='From {}'.format(reaction.message.channel.name), color=0xcd1ff8)
        embed.set_author(name=reaction.message.author.name,
                         icon_url=reaction.message.author.avatar_url or reaction.message.author.default_avatar_url)
        embed.set_footer(text=reaction.message.id)

        await client.send_message(bd_starboard, content=reaction.message.content, embed=embed)


@client.event
async def on_message(message):
    # don't respond to yourself, silly
    if message.author.id == client.user.id:
        return

    if message.content.startswith('!roll'):
        await client.send_message(message.channel,
                                  '{0.author.mention} :game_die:\n{1}'.format(message, roll(message.content[5:])))
    elif message.content.startswith('!xroll'):
        await client.send_message(message.channel,
                                  '{0.author.mention} :game_die:\n{1}'.format(message, xroll(message.content[6:])))
    elif message.content.startswith('!multiroll'):
        await client.send_message(message.channel,
                                  '{0.author.mention} :game_die:\n{1}'.format(message, multiroll(message.content)))
    elif message.content.startswith('!ping'):
        await client.send_message(message.channel, 'pong')
    elif message.content.startswith('!help'):
        await help(message)
    elif message.content.startswith('!choose'):
        await client.send_message(message.channel,
                                  '{0.author.mention} :game_die:\n{1}'.format(message, choose(message.content)))
    elif message.content.startswith('!misfire'):
        await client.send_message(message.channel,
                                  '{0.author.mention} :game_die:\n{1}'.format(message, misfire(message.content)))
    elif message.content.startswith('!dof'):
        await client.send_message(message.channel,
                                  '{0.author.mention} :game_die:\n{1}'.format(message, dof(message.content)))
    elif message.content.startswith('!ims'):
        await client.send_message(message.channel,
                                  '{0.author.mention}\n{1}'.format(message, ims(message.content)))
    elif message.content.startswith('!ptgs'):
        await client.send_message(message.channel,
                                  '{0.author.mention}\n{1}'.format(message, ptgs(message.content)))
    elif message.content.startswith('!csload'):
        resp = await csload(message)
        await client.send_message(message.channel,
                                  '{0.author.mention}\n{1}'.format(message, resp))
    elif message.content.startswith('!csroll') or message.content.startswith('!csxroll'):
        await client.send_message(message.channel,
                                  '{0.author.mention}\n{1}'.format(message, csroll(message)))
    elif message.content.startswith('!cs'):
        await client.send_message(message.channel,
                                  '{0.author.mention}\n{1}'.format(message, cs(message)))
    elif message.content.startswith('!odds'):
        await client.send_message(message.channel,
                                  '{0.author.mention}\n{1}'.format(message, odds(message.content[5:])))


if __name__ == '__main__':
    while True:
        try:
            from time import sleep
            client.run(os.environ['DISCORD_TOKEN'])
            sleep(5)
        except ConnectionResetError:
            sleep(5)
        except StopIteration:
            break
